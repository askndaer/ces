import { NavigationContainer } from '@react-navigation/native';
import React, { useMemo, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { AppContext } from './src/context/AppContext';
import { UserContext } from './src/context/UserContext';
import Router from './src/navigation/routes/index';



const App = () => {
  const [AppInf, setAppInfo] = useState({});
  const AppInformation = useMemo(() => ({ AppInf, setAppInfo }), [AppInf, setAppInfo]);

  const [user, setUser] = useState([]);
  const users = useMemo(() => ({ user, setUser }), [user, setUser]);

  return (
    <View style={styles.container}>
      <AppContext.Provider value={AppInformation}>
        <UserContext.Provider value={users}>
          <NavigationContainer style={{ flex: 1 }} >
            <Router />
          </NavigationContainer>
        </UserContext.Provider>
      </AppContext.Provider>
    </View>
  )
}

export default App

const styles = StyleSheet.create({
  container: {
    height: "100%",
    flex: 1,
    width: "100%",


  }
})