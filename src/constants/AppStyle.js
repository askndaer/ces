import { StyleSheet } from "react-native";

export const TextStyle = StyleSheet.create({

    h1: {
        fontSize: 24,
        color: "#fff",
        fontFamily: 'Cairo-Bold'
    },
    h2: {
        fontSize: 16, color: "#535c68", fontFamily: 'Cairo-Bold'
    },
    h2Light: {
        fontSize: 16, color: "#535c68", fontFamily: 'Cairo-Light'
    }

})

