import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import Albums from './../../../screen/Albums';
import UserDisplay from './../../../screen/UserDisplay';
import UserInfo from './../../../screen/UserInfo';
import { USER_ALBUMS, USER_DISPLAY, USER_INFO } from './../../navigationConstant';
const Stack = createStackNavigator();

export default function UserStack() {
    return (
        <Stack.Navigator initialRouteName={USER_INFO} screenOptions={{ headerShown: false }}>
            <Stack.Screen
                name={USER_INFO}
                component={UserInfo}
                options={{
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name={USER_ALBUMS}
                component={Albums}
                options={{
                    gestureEnabled: false,
                }}
            />

            <Stack.Screen
                name={USER_DISPLAY}
                component={UserDisplay}
                options={{
                    gestureEnabled: false,
                }}
            />


        </Stack.Navigator>
    );
}
