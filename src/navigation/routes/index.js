import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { USER_STACK } from '../navigationConstant';
import UserStack from './user/UserStack';


const routesConfig = [
    {
        name: USER_STACK,
        component: UserStack,
        options: { animationEnabled: false },
    },

];

const Stack = createStackNavigator();
function Router() {
    return (
        <Stack.Navigator
            initialRouteName={USER_STACK}
            screenOptions={{ headerShown: false }}
            options={{ gestureEnabled: false }}>
            {routesConfig.map((route, i) => (
                <Stack.Screen
                    key={route.name}
                    name={route.name}
                    component={route.component}
                    options={route.options || {}}
                />
            ))}
        </Stack.Navigator>
    );
}

export default Router;
