import Lottie from 'lottie-react-native';
import React from 'react';
import { StyleSheet, View } from 'react-native';
const Loading = () => {
    return (
        <View style={styles.container}>
            <Lottie source={require('../assets/lottiFile/loading.json')} autoPlay loop />
        </View>
    )
}

export default Loading

const styles = StyleSheet.create({

    container: {
        position: 'absolute',
        height: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        width: "100%",
        backgroundColor: "rgba(0, 0, 0, 0.45)"

    }
})