// import { MEDIUM_GREY } from '@colors';
import { BlurView } from "@react-native-community/blur";
import Lottie from 'lottie-react-native';
import React from 'react';
import { SafeAreaView, StatusBar, StyleSheet, View } from 'react-native';
import { MEDIUM_GREY } from "../constants/colors";

const Screen = ({ children, showLoaderModal }) => {
    return (

        <View style={[styles.container,]}>
            <SafeAreaView style={styles.SafeAreaViewContainer}>
                {children}
            </SafeAreaView>

            {showLoaderModal &&
                <>
                    < BlurView
                        style={styles.absolute}
                        blurType="light"
                        blurAmount={10}
                        reducedTransparencyFallbackColor="white"
                    />
                    <View style={[styles.absolute, {
                        justifyContent: 'center',
                        alignItems: 'center',

                    }]}>
                        <Lottie
                            source={require('../assets/lottiFile/loading.json')}
                            style={styles.loading}

                            autoPlay
                            loop
                        />

                    </View>
                </>
            }
        </View >

    )
}

export default Screen

Screen.defaultProps = {
    backgroundColor: MEDIUM_GREY,
    showLoaderModal: false,

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    SafeAreaViewContainer: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        padding: 20
    },

    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    loading: {
        width: 100,
        height: 100,
    }
})
