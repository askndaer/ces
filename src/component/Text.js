import React from 'react';
import { StyleSheet, Text as NativeText } from 'react-native';
import { TextStyle } from './../constants/AppStyle';

const Text = (props) => {
    return (
        <NativeText
            {...props}
            style={[TextStyle[props.TextStyle], props.style]}
        > {props.children}</NativeText>
    )
}

export default Text

Text.defaultProps = {
    translate: true,
    TextStyle: 'h1'
};


const styles = StyleSheet.create({})