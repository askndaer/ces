import React, { useEffect, useState } from 'react'
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { fetchUserPhotos } from '../api/users'
import Fade from "../component/Animations/Fade"
import Screen from '../component/ScreenContainer'
import { USER_DISPLAY } from './../navigation/navigationConstant'
const Albums = (props) => {

    const { id } = props?.route?.params?.item

    const [UIData, setUIData] = useState(
        {
            Loading: true,
            userPhotos: [[]]
        }
    )

    // const [Loading, setLoading] = useState(false)
    // const [userPhotos, setUserPhotos] = useState({})

    const UserImage = async () => {

        const { status, data } = await fetchUserPhotos(id);
        console.log(data)
        if (status) {
            setUIData({
                Loading: false,
                userPhotos: data
            })

        } else {

        }

    }

    useEffect(() => {
        UserImage()
    }, []);


    const Item = ({ item, item: { thumbnailUrl, title } }) => (
        <Fade
            fadeMode="fadeIn"
            duration={800}
            delay={100}

        >
            <TouchableOpacity
                onPress={() => props.navigation.navigate(USER_DISPLAY, { item: item, UserID: id })}
                style={{ height: 140, backgroundColor: "#fff", marginVertical: 10, padding: 20, borderRadius: 10, overflow: 'hidden' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                    <Image


                        style={{ height: 100, width: 100, borderRadius: 10, marginRight: 10 }}
                        source={{ uri: thumbnailUrl }} />
                    <Text
                        style={{ fontSize: 18, color: "#535c68", flex: 1, fontFamily: 'Cairo-Bold' }}
                    >{title}</Text>


                </View>
            </TouchableOpacity>
        </Fade>
    );
    const renderItem = ({ item }) => (<>
        <Item item={item} />
    </>
    );


    return (
        <Screen showLoaderModal={UIData?.Loading}>

            <FlatList
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                data={UIData?.userPhotos}
                renderItem={renderItem}
                keyExtractor={item => item?.id}

            />

        </Screen>
    )
}

export default Albums

const styles = StyleSheet.create({})