import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import Fade from "../component/Animations/Fade"
import Screen from '../component/ScreenContainer'
const UserDisplay = (props) => {

    const { albumId, url, title } = props?.route?.params?.item
    const { UserID } = props?.route?.params



    return (
        <Screen >

            <Fade
                fadeMode="fadeIn"
                duration={800}
                delay={100}

            >

                <Text
                    style={{ fontSize: 18, color: "#535c68", fontFamily: 'Cairo-Bold' }}
                >{`UserID :${UserID}`}</Text>

                <Text
                    style={{ fontSize: 18, color: "#535c68", fontFamily: 'Cairo-Bold' }}
                >{`Phone ID :${albumId}`}</Text>
                <View style={{ flex: 1, borderRadius: 10, marginRight: 10 }}>
                    <Image


                        style={{ flexGrow: 1, borderRadius: 10 }}
                        source={{ uri: url }} />
                </View>
                <View style={{ justifyContent: "center", alignItems: 'center', borderRadius: 20, backgroundColor: "#3498db", height: 100, marginTop: 20 }}>
                    <Text
                        style={{ fontSize: 18, color: "#fff", fontFamily: 'Cairo-Bold' }}
                    >{title}</Text>
                </View>
            </Fade>
        </Screen>
    )
}

export default UserDisplay

const styles = StyleSheet.create({})