import Lottie from 'lottie-react-native';
import React, { useContext, useEffect, useState } from 'react';
import { FlatList, RefreshControl, TouchableOpacity, View } from 'react-native';
import { fetchUser } from '../api/users';
import Screen from '../component/ScreenContainer';
import Text from '../component/Text';
import { UserContext } from '../context/UserContext';
import { USER_ALBUMS } from '../navigation/navigationConstant';

const UserInfo = (props) => {
    const { user, setUser } = useContext(UserContext);
    const [Loading, setLoading] = useState(false)
    const [refreshing, setRefreshing] = useState(false)




    const UserList = async () => {
        setLoading(true)
        const { status, data } = await fetchUser();
        if (status) {
            setUser(data)
            setLoading(false)
        } else {

        }
    }

    useEffect(() => {
        UserList()
    }, []);

    const Item = ({ item, item: { name, email, phone, id } }) => (
        <TouchableOpacity
            onPress={() => props.navigation.navigate(USER_ALBUMS, { item: item })}
            style={{ height: 140, backgroundColor: "#fff", marginVertical: 10, padding: 20, borderRadius: 10, overflow: 'hidden' }}>

            <Lottie

                speed={Math.floor(Math.random() * 2) + 1 * .2}
                style={{ top: -50, height: 250, backgroundColor: "#fff", marginVertical: 20, borderRadius: 10, position: 'absolute', opacity: .8 }}
                source={require('../assets/lottiFile/background3.json')} autoPlay loop />
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View>
                    <Text TextStyle={'h1'}>{name}</Text>
                    <Text TextStyle={'h2'}>{email}</Text>
                    <Text TextStyle={'h2Light'}>{phone}</Text>
                </View>
                <View>
                    <Text
                        style={{ fontSize: 50, color: "#ecf0f1", fontFamily: 'Cairo-Bold' }}
                    >{id}</Text>
                </View>
            </View>
        </TouchableOpacity >
    );
    const renderItem = ({ item }) => (<>
        <Item item={item} />
    </>
    );


    return (
        <Screen showLoaderModal={Loading}>
            <Text
                style={{ fontSize: 26, color: "#e67e22", fontFamily: 'Cairo-Bold' }}
            >User Information</Text>
            <FlatList
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                data={user}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={UserList}
                    />
                }
            />
        </Screen>
    )
}

export default UserInfo