
import { getRequest } from './APIClient';


export async function fetchUser() {
    try {
        const response = await getRequest('users');
        return { status: true, data: response.data }
    } catch (error) {
        return { status: false, data: response.data }
    }
}

export async function fetchUserPhotos(userID) {
    try {
        const response = await getRequest(`photos?id=${userID}`);
        return { status: true, data: response.data }
    } catch (error) {
        return { status: false, data: response }
    }
}

